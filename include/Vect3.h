#pragma once
#ifndef VECT3_H
#define VECT3_H

#include <math.h>
#include <stdlib.h>
#include <iostream>

class vect3
{
public:
	vect3(){}
	vect3(float e0, float e1, float e2) 
	{
		e[0] = e0;
		e[1] = e1;
		e[2] = e2;
	}

	inline float x() const { return e[0]; }
	inline float y() const { return e[1]; }
	inline float z() const { return e[2]; }
	inline float r() const { return e[0]; }
	inline float g() const { return e[1]; }
	inline float b() const { return e[2]; }

	inline const vect3& operator+() const { return *this; }
	inline vect3  operator-() const { return vect3(e[0], e[1], e[2]); }
	inline float  operator[](int i) const { return e[i]; }
	inline float& operator[](int i) { return e[i]; }

	inline vect3& operator+=(const vect3 &v2);
	inline vect3& operator-=(const vect3 &v2);
	inline vect3& operator*=(const vect3 &v2);
	inline vect3& operator/=(const vect3 &v2);
	inline vect3& operator*=(const float t);
	inline vect3& operator/=(const float t);

	inline float lenght() const
	{
		return sqrt(e[0]*e[0] + e[1]*e[1] + e[2]*e[2]);
	}

	inline float squared_lenght() const
	{
		return e[0] * e[0] + e[1] * e[1] + e[2] * e[2];
	}

	inline void make_unit_vector();

public:
	float e[3];
};
#endif // !VECT3_H
