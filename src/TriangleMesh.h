#pragma once
#include <fstream>
#include <istream>
#include <iostream>
#include <string>
#include "MRTMath.h"

#define TINYOBJLOADER_IMPLEMENTATION
#include "../include/tiny_obj_loader.h"
using namespace tinyobj;

//Funcion Global
bool rayTriangleIntersect(const Vec3f &o, const Vec3f &d, const Vec3f &v0, const Vec3f &v1, const Vec3f &v2, float &t, const Vec2f &u, const Vec2f &v)
{
	return false;
}




/////////////////////////////////////////////////////////////////////////
//Class TriangleMesh: 
// Realiza carga de mallas poligonales trianguladas OBJ 
// TODO: incluir formato de referencia .geo (www.scratchapixel.com) 
//       y mallas OBJ con caras tipo quads. 
/////////////////////////////////////////////////////////////////////////
__declspec(align(16)) class TriangleMesh :public Object
{
public:

	//TriangleMesh(const uint32_t nFaces,
	//	const std::unique_ptr<uint32_t[]> &faceIndex,
	//	const std::unique_ptr<uint32_t[]> &vertexIndex,
	//	const std::unique_ptr<Vec3[]>     &vertices,
	//	const std::unique_ptr<Vec3[]>     &normals,
	//	const std::unique_ptr<Vec2[]>     &uv);

	TriangleMesh(std::vector<uint32_t>  &faceIndex,
		std::vector<uint32_t>	&vertexIndex,
		std::vector<Vec3f>	&vertices,
		std::vector<Vec3f>	&faceNormals,
		std::vector<Vec3f>	&vertexNormals,
		std::vector<Vec2f>	&uv);

	void setFaceNormals(const std::vector<Vec3f> &fn);
	void setVertexNormals(std::vector<Vec3f> &vn);
	Matrix4f rotationMatrix(float ax, float ay, float az);
	void transformPoint(const std::vector<Vec3f> &p, std::vector<Vec3f> &out, const Matrix4f &m);
	void transformNormals(const std::vector<Vec3f> &n, std::vector<Vec3f> &outn, const Matrix4f &m);
	void transformVectors(const std::vector<Vec3f> &v, std::vector<Vec3f> &outv, const Matrix4f &m);
	void normalize(std::vector<Vec3f> &n);
	virtual Vec3f* traceRayToObject(const Ray &r, float &tNear) const;
	virtual bool intersect(const Vec3f &o, const Vec3f &d, float &tNear,const uint32_t &vIdx,const uint32_t &fidx, Vec2f &uv) const;
	virtual bool intersectBarycentric(const Vec3f &o, const Vec3f &d, float &tNear, const uint32_t &vidx, const uint32_t &fidx, Vec2f &uv) const;
	virtual void getSurfaceProperties(const Vec3f &, const Vec3f &, const uint32_t &, const Vec2f &, Vec3f &, Vec2f &) const;
	static TriangleMesh* loadPolyMeshFromFile(const char *file);
	static TriangleMesh* loadPolyMeshOBJ(const std::string file);
	static std::vector<Vec3f> calcFaceNormals(const std::vector<Vec3f> &vertices, const std::vector<uint32_t> &triangleIndices);
	std::vector<Vec3f> calcVertexNormals(std::vector<Vec3f> &vertices);

	void* operator new(size_t i)
	{
		return _mm_malloc(i, 16);
	}

	void operator delete(void* p)
	{
		_mm_free(p);
	}

private:
	uint32_t               _numTriangles;	
	//reemplazo std::unique_ptr<uint32_t[]>
	std::vector<uint32_t> _faceIndex;
	std::vector<uint32_t> _triangleIndex;
	std::vector<Vec3f>     _vertices;
	std::vector<uint32_t> _vertexIndex;
	std::vector<Vec3f>     _faceNormals;
	std::vector<Vec3f>     _faceNormalIndex;
	std::vector<Vec3f>     _vertexNormals;
	std::vector<uint32_t> _vertexNormalIndex;
	std::vector<Vec2f>     _uv;
	Matrix4f		  _rotMatrix;
};





///////////////////////////////////////////////
//Constructor para mallas con caras cuadradas
//////////////////////////////////////////////
/*
TriangleMesh::TriangleMesh(const uint32_t nFaces,
	const std::unique_ptr<uint32_t[]> &faceIndex,
	const std::unique_ptr<uint32_t[]> &vertexIndex,
	const std::unique_ptr<Vec3[]>     &vertices,
	const std::unique_ptr<Vec3[]>     &normals,
	const std::unique_ptr<Vec2[]>     &uv)
{
	/*
	_numTriangles = 0;
	uint32_t k = 0, maxVertIndex = 0;
	for (uint32_t i = 0; i < nFaces; ++i)
	{
		_numTriangles += faceIndex[i] - 2;
		for (uint32_t j = 0; j < faceIndex[i]; ++j)
		{
			if (vertexIndex[k + j] > maxVertIndex)
				maxVertIndex = vertexIndex[k + j];
			k += vertexIndex[i];
		}
	}
	maxVertIndex += 1;

	//Separar memoria para los vertices 
	_vertices = std::unique_ptr<Vec3[]>(new Vec3[maxVertIndex]);
	for (uint32_t i = 0; i < maxVertIndex; ++i)
	{
		_vertices[i] = vertices[i];
	}

	//Separar memoria para indices de triangulos 
	_triangleIndex = std::unique_ptr<uint32_t[]>(new uint32_t[maxVertIndex * 3]);
	uint32_t l = 0;

	_faceNormals = std::unique_ptr<Vec3[]>(new Vec3[_numTriangles * 3]);
	for (uint32_t i = 0, k = 0; i < nFaces; ++i)//Para cada cara
	{
		for (uint32_t j = 0; j < faceIndex[i] - 2; ++j)////Para cada triangulo en la cara
		{
			_triangleIndex[l] = vertexIndex[k];
			_triangleIndex[l + 1] = vertexIndex[k + j + 1];
			_triangleIndex[l + 2] = vertexIndex[k + j + 2];
			_faceNormals[l] = normals[k];
			_faceNormals[l + 1] = normals[k + j + 1];
			_faceNormals[l + 2] = normals[k + j + 2];
			_uv[l] = uv[k];
			_uv[l + 1] = uv[k + j + 1];
			_uv[l + 2] = uv[k + j + 2];
		}
		k += faceIndex[i];
	}
	
}
*/


//////////////////////////////////////////////
//Carga de malla ya triangulada
//////////////////////////////////////////////
TriangleMesh::TriangleMesh(std::vector<uint32_t>  &faceIndex,
							std::vector<uint32_t>	&vertexIndex,
							std::vector<Vec3f>	&vertices,
							std::vector<Vec3f>	&faceNormals,
							std::vector<Vec3f>	&vertexNormals,
							std::vector<Vec2f>	&uv)

{
	uint32_t k = 0, maxVertIndex = 0;

	_triangleIndex =  faceIndex;
	_vertexIndex = vertexIndex;
	_vertices = vertices;
	_faceNormals = faceNormals;
	_uv = uv;	
	_numTriangles = vertexIndex.size()/3;
	//maxVertIndex = 0;	
}


///////////////////////////////////////////////////
//Crear normales por cara a partir de los vertices
///////////////////////////////////////////////////
std::vector<Vec3f> TriangleMesh::calcFaceNormals(const std::vector<Vec3f> &vertices, const std::vector<uint32_t> &triangleIndices)
{
	
	std::vector<Vec3f> faceNormals;
	for (size_t i = 0; i < (vertices.size()/3); i++)
	{
		Vec3f v0 = vertices[triangleIndices[i * 3 + 0]];
		Vec3f A = vertices[triangleIndices[i * 3 + 0]] - vertices[triangleIndices[i * 3 + 1]];
		Vec3f B = vertices[triangleIndices[i * 3 + 0]] - vertices[triangleIndices[i * 3 + 2]];
		Vec3f N = A.crossProduct(B);
		
#ifdef MRT_DEBUG
		std::cout << "Triangle (" << vertices[triangleIndices[i * 3 + 0]].x() << ","
			<< vertices[triangleIndices[i * 3 + 0]].y() << ","
			<< vertices[triangleIndices[i * 3 + 0]].z() << ") , ";
		std::cout << "(" << vertices[triangleIndices[i * 3 + 1]].x() << ","
			<< vertices[triangleIndices[i * 3 + 1]].y() << ","
			<< vertices[triangleIndices[i * 3 + 1]].z() << ") , ";
		std::cout << "(" << vertices[triangleIndices[i * 3 + 2]].x() << ","
			<< vertices[triangleIndices[i * 3 + 2]].y() << ","
			<< vertices[triangleIndices[i * 3 + 2]].z() << ")\n";
#endif //MRT_DEBUG
		std::cout << "Normal (" << N.x << ","
			<< N.y << ","
			<< N.z << ")\n";
		N.normalize();
		std::cout << "Normalized Normal (" << N.x << ","
			<< N.y << ","
			<< N.z << ")\n";
		faceNormals.push_back(N);
		
	}
	std::cout << "Total face Normals: "<< faceNormals.size();
	return faceNormals;
}//calcFaceNormals


//////////////////////////////////////////////
//Crear normales por vertice
//////////////////////////////////////////////
std::vector<Vec3f> TriangleMesh::calcVertexNormals(std::vector<Vec3f> &vn)
{

}


//////////////////////////////////////////////
//Normales por cara a partir de los vertices
//////////////////////////////////////////////
void TriangleMesh::setFaceNormals(const std::vector<Vec3f> &fn)
{
	_faceNormals = fn;
}


//////////////////////////////////////////////
//Normales por vertice
//////////////////////////////////////////////
void TriangleMesh::setVertexNormals(std::vector<Vec3f> &vn)
{
	_vertexNormals = vn;
}


//////////////////////////////////////////////
//Normalizar un conjunto 
//////////////////////////////////////////////
void TriangleMesh::normalize(std::vector<Vec3f> &n)
{
	for (size_t i = 0; i < n.size(); ++i)
	{
		n[i].normalize();
	}
}


//////////////////////////////////////////////
//Obtener matriz de rotac�on
//////////////////////////////////////////////
Matrix4f TriangleMesh::rotationMatrix(float ax, float ay, float az)
{
	/*
	Eigen::Matrix4f rx =
		Eigen::Matrix4f(Eigen::AngleAxisf(ax, Eigen::Vector3f(1, 0, 0, 0)));
	Eigen::Matrix4f ry =
		Eigen::Matrix4f(Eigen::AngleAxisf(ay, Eigen::Vector3f(0, 1, 0, 0)));
	Eigen::Matrix4f rz =
		Eigen::Matrix4f(Eigen::AngleAxisf(az, Eigen::Vector3f(0, 0, 1, 0)));
	Eigen::Matrix4f rw =
		Eigen::Matrix4f(Eigen::AngleAxisf(1, Eigen::Vector3f(0, 0, 0, 1)));
	return rz * ry * rx * rw;
	*/
}


//////////////////////////////////////////////
//Transformar puntos
//////////////////////////////////////////////
void TriangleMesh::transformPoint(const std::vector<Vec3f> &p, std::vector<Vec3f> &out, const Matrix4f &m)
{ 
	/*
	for (size_t i = 0; i < p.size(); ++i)
	{
		float tmpxb = p[i].x() * m(0, 0) + p[i].y() * m(1, 0) + p[i].x() * m(2, 0) + m(3, 0);
		out[i].x()  = p[i].x() * m(0, 0) + p[i].y() * m(1, 0) + p[i].x() * m(2, 0) + m(3, 0);
		out[i].y()  = p[i].x() * m(0, 1) + p[i].y() * m(1, 1) + p[i].x() * m(2, 1) + m(3, 1);
		out[i].z()  = p[i].x() * m(0, 2) + p[i].y() * m(1, 2) + p[i].x() * m(2, 2) + m(3, 2);
		out[i].w() = p[i].x() * m(0, 3) + p[i].y() * m(1, 3) + p[i].x() * m(2, 3) + m(3, 3);
		
		if (out[i].w() != 1)
		{
			out[i].x() /= out[i].w();
			out[i].y() /= out[i].w();
			out[i].z() /= out[i].w();
		}

	}
	*/
}


//////////////////////////////////////////////
//Transformar normales
//////////////////////////////////////////////
void TriangleMesh::transformNormals(const std::vector<Vec3f> &n, std::vector<Vec3f> &outn, const Matrix4f &m)
{	
	/*
	std::vector<Vec3f> tempNormals;
	//n[0] * m;
	
	Matrix4f minv = m.inverse();
	for (size_t i = 0; i < n.size(); ++i)
	{
		outn[i].x() = n[i].x() *minv(0,0) + n[i].y() *minv(1,0) + n[i].x() *minv(2,0) +minv(3,0);
		outn[i].y() = n[i].x() *minv(0,1) + n[i].y() *minv(1,1) + n[i].x() *minv(2,1) +minv(3,1);
		outn[i].z() = n[i].x() *minv(0,2) + n[i].y() *minv(1,2) + n[i].x() *minv(2,2) +minv(3,2);
		//tempNormals.push_back(n[i]*m.transpose().inverse());
	}	
	*/
}


//////////////////////////////////////////////
//Transformar vectores
//////////////////////////////////////////////
void TriangleMesh::transformVectors(const std::vector<Vec3f> &v,  std::vector<Vec3f> &outv, const Matrix4f &m)
{ 
	/*
	for (size_t i = 0; i < v.size(); ++i)
	{
		outv[i].x() = v[i].x() * m(0,0) + v[i].y() * m(1,0) + v[i].x() * m(2,0) + m(3,0);
		outv[i].y() = v[i].x() * m(0,1) + v[i].y() * m(1,1) + v[i].x() * m(2,1) + m(3,1);
		outv[i].z() = v[i].x() * m(0,2) + v[i].y() * m(1,2) + v[i].x() * m(2,2) + m(3,2);
	}
	*/
} 


////////////////////////////////////////////////////////////////
//M�todo abstracto implementado para comprobar intersecci�n de
//rayo proveniente de c�mara a mallas triangulares
////////////////////////////////////////////////////////////////
 Vec3f* TriangleMesh::traceRayToObject(const Ray &r, float &tNear) const
{
	 Vec2f uv;
	 Vec3f color;
	 for (size_t i = 0; i < (_vertexIndex.size()/3); i++)
	 {		  
		 uint32_t vidx = _vertexIndex[i*3];
		 if(_uv.size() > 0) uv = _uv[i];
		 //color;
		 //if(!intersect(r.o, r.d, tNear, vidx, uv)) return new Vec3(0.0f,0.0f,0.0f);
		 if (intersect(r.o, r.d, tNear, vidx,i, uv))
			 std::cout << "hit\n";
		 //	 std::cout << "no hit";
		 //else
			 
		 //const  Vec3f x = r.o + r.d * tNear, n = (x - sph.p).normalized(), w = -r.d;
	 }
	 return nullptr;
}


/////////////////////////////////////////////////////
//Sobreescritua de intersect para dtecci�n de 
//intersecci�n del rayo con el triangulo (eq. plano)
/////////////////////////////////////////////////////
bool TriangleMesh::intersect(const Vec3f &o, const Vec3f &d, float &tNear, const uint32_t &vidx, const uint32_t &fidx, Vec2f &uv) const
{	
	Vec3f N = _faceNormals[fidx];
	float ndotdir = N.dotProduct(d);
	
	//paralelos, no se intersectan
	if (ndotdir < 0) return false; 
	float D = _vertices[vidx].dotProduct(N);
	float t = -(N.dotProduct(o) + D) / ndotdir;
	
	//comprobar rayo detras del triangulo
	if (t < 0) return false;
	//_faceNormals[vidx * 3 + 0];
	Vec3f p = o + t * d;

	//�Se eval�a si p est� dentro del triangulo edge0, edge1, edge2
	Vec3f C;

	Vec3f edge0 = _vertices[vidx + 1] - _vertices[vidx + 0];
	C = p - _vertices[vidx + 0];
	if (N.dotProduct(edge0.crossProduct(C)) < 0) return false;
	
	Vec3f edge1 = _vertices[vidx + 2] - _vertices[vidx + 1];
	C = p - _vertices[vidx + 1];
	if (N.dotProduct(edge1.crossProduct(C)) < 0) return false;

	Vec3f edge2 = _vertices[vidx + 0] - _vertices[vidx + 2];	
	C = p - _vertices[vidx + 2];
	if (N.dotProduct(edge2.crossProduct(C)) < 0) return false;
		
	return true;
}


//////////////////////////////////////////////////////////
//Sobreescritua de intersectBarycentric para detecci�n de 
//intersecci�n del rayo con el triangulo encontrando el 
//centro del triangulo mediante 3 escalares
//////////////////////////////////////////////////////////
bool TriangleMesh::intersectBarycentric(const Vec3f &o, const Vec3f &d, float &tNear, const uint32_t &vidx, const uint32_t &fidx, Vec2f &uv) const
{
	Vec3f N = _faceNormals[fidx];
	float ndotdir = N.dotProduct(d);

	//paralelos, no se intersectan
	if (ndotdir < 0) return false;
	float D = _vertices[vidx].dotProduct(N);
	float t = -(N.dotProduct(o) + D) / ndotdir;

	//comprobar rayo detras del triangulo
	if (t < 0) return false;
	//_faceNormals[vidx * 3 + 0];
	Vec3f p = o + t * d;

	//�Se eval�a si p est� dentro del triangulo edge0, edge1, edge2
	Vec3f C;

	Vec3f edge0 = _vertices[vidx + 1] - _vertices[vidx + 0];
	C = p - _vertices[vidx + 0];
	if (N.dotProduct(edge0.crossProduct(C)) < 0) return false;

	Vec3f edge1 = _vertices[vidx + 2] - _vertices[vidx + 1];
	C = p - _vertices[vidx + 1];
	if (N.dotProduct(edge1.crossProduct(C)) < 0) return false;

	Vec3f edge2 = _vertices[vidx + 0] - _vertices[vidx + 2];
	C = p - _vertices[vidx + 2];
	if (N.dotProduct(edge2.crossProduct(C)) < 0) return false;

	return true;
}


/////////////////////////////////////////////////////
//Propiedades de superficie
/////////////////////////////////////////////////////
void TriangleMesh::getSurfaceProperties(const Vec3f &, const Vec3f &, const uint32_t &, const Vec2f &, Vec3f &, Vec2f &) const
{

}


//////////////////////////////////////////////
//Carga de PolyMesh Quads
//////////////////////////////////////////////
TriangleMesh* TriangleMesh::loadPolyMeshFromFile(const char *file)
{
	/*
	std::ifstream ifs;
	try {
		ifs.open(file);
		if (ifs.fail()) throw;
		std::stringstream ss;
		ss << ifs.rdbuf();
		uint32_t numFaces;
		ss >> numFaces;
		std::unique_ptr<uint32_t[]> faceIndex(new uint32_t[numFaces]);
		uint32_t vertsIndexArraySize = 0;
		// reading face index array
		for (uint32_t i = 0; i < numFaces; ++i) {
			ss >> faceIndex[i];
			vertsIndexArraySize += faceIndex[i];
		}
		std::unique_ptr<uint32_t[]> vertsIndex(new uint32_t[vertsIndexArraySize]);
		uint32_t vertsArraySize = 0;
		// reading vertex index array
		for (uint32_t i = 0; i < vertsIndexArraySize; ++i) {
			ss >> vertsIndex[i];
			if (vertsIndex[i] > vertsArraySize) vertsArraySize = vertsIndex[i];
		}
		vertsArraySize += 1;
		// reading vertices
		std::unique_ptr<Vec3[]> verts(new Vec3[vertsArraySize]);
		for (uint32_t i = 0; i < vertsArraySize; ++i) {
			ss >> verts[i].x >> verts[i].y >> verts[i].z;
		}
		// reading normals
		std::unique_ptr<Vec3[]> normals(new Vec3[vertsIndexArraySize]);
		for (uint32_t i = 0; i < vertsIndexArraySize; ++i) {
			ss >> normals[i].x >> normals[i].y >> normals[i].z;
		}
		// reading st coordinates
		std::unique_ptr<Vec2[]> st(new Vec2[vertsIndexArraySize]);
		for (uint32_t i = 0; i < vertsIndexArraySize; ++i) {
			ss >> st[i].x >> st[i].y;
		}

		return new TriangleMesh(numFaces, faceIndex, vertsIndex, verts, normals, st);
	}
	catch (std::exception const& e) {
		ifs.close();
	}
	ifs.close();
	*/
	return nullptr;

}


//////////////////////////////////////////////
//Carga de modelo OBJ (basado en triangulos)
//////////////////////////////////////////////
TriangleMesh* TriangleMesh::loadPolyMeshOBJ(const std::string file)
{
	std::ifstream ifs;
	std::size_t tokenPos = file.find_last_of('.');
	std::string fileExt = file.substr(tokenPos + 1);
	std::cout << "Archivo a cargar: " << file << "\n";
	std::cout << "Extension de archivo: " << fileExt << "\n";

	if (fileExt != "obj" && fileExt != "OBJ")
	{
		std::cout << "Error en extension de archivo, se esperaba .obj,  ";
		exit(0);
	}
	try
	{
		ifs.open(file.c_str());
		if (ifs.fail())
		{
			std::cout << "Error en extension de archivo, se esperaba .obj,  ";
			throw;
		}
		ifs.close();

		tinyobj::attrib_t attrib;
		std::vector<tinyobj::shape_t> shapes;
		std::vector<tinyobj::material_t> materials;
		std::string warn, err;

		if (!tinyobj::LoadObj(&attrib, &shapes, &materials, &warn, &err, file.c_str()))
		{
			throw std::runtime_error(warn + err);
		}

		printf("# Elementos de archivo OBJ  = %d\n\n", (int)(attrib.vertices.size()) / 3);
		printf("# of vertices  = %d\n", (int)(attrib.vertices.size()) / 3);
		printf("# of normals   = %d\n", (int)(attrib.normals.size()) / 3);
		printf("# of texcoords = %d\n", (int)(attrib.texcoords.size()) / 2);
		printf("# of materials = %d\n", (int)materials.size());
		printf("# of shapes    = %d\n", (int)shapes.size());
		printf("# of shape name    = %s\n", shapes[0].name.c_str());
		//printf("# of mesh tags   = %d\n", shapes[0].mesh.tags);
		printf("# of mesh faces   = %d\n", (shapes[0].mesh.indices.size() / 3));

		uint32_t tmpMaxVertIndex = 0;
		//std::unique_ptr<Vec3[]> tmpVertices = std::unique_ptr<Vec3[]>(new Vec3[shapes.size()]);
		//std::unique_ptr<Vec3[]> tmpNumTriangles = 0;
		//std::unique_ptr<Vec3[]> tmpVertexIndices = std::unique_ptr<Vec3[]>(new Vec3[(shapes[0].mesh.indices.size() / 3)]);
		//std::unique_ptr<Vec3[]> tmpVertexNormals = std::unique_ptr<Vec3[]>(new Vec3[attrib.normals.size()]);
		//std::unique_ptr<Vec2[]> tmpuv = std::unique_ptr<Vec2[]>(new Vec2[attrib.texcoords.size()]);

		std::vector<Vec3f> tmpVertices;
		std::vector<Vec3f> tmpNumTriangles;
		std::vector<Vec3f> tmpVertexNormals;
		std::vector<uint32_t> tmpVertexIndices;
		std::vector<uint32_t> tmpNomalIndices;
		std::vector<uint32_t> tmpUVIndices;
		std::vector<Vec2f> tmpuv;

		//Carga de vertices
		for (uint32_t i = 0; i < (attrib.vertices.size() / 3); ++i)
		{
			//tmpVertices[i][0] = attrib.vertices[3 * i + 0];
			//tmpVertices[i][1] = attrib.vertices[3 * i + 1];
			//tmpVertices[i][2] = attrib.vertices[3 * i + 2];
			Vec3f vert0 = {};			
			vert0[0] = attrib.vertices[3 * i + 0];
			vert0[1] = attrib.vertices[3 * i + 1];
			vert0[2] = attrib.vertices[3 * i + 2];
			tmpVertices.push_back(vert0);
			
			//std::cout << i << " Tmp Vertices: " << "(" << tmpVertices[i][0] << "," << tmpVertices[i][1] << "," << tmpVertices[i][2] << ")" << "\n";
			//std::cout << " Tmp vertices: " << "(" << attrib.vertices[3 * i + 0] << "," << attrib.vertices[3 * i + 1] << "," << attrib.vertices[3 * i + 2] << ")" << "\n";
		}

		//Carga de normales
		for (uint32_t i = 0; i < (attrib.normals.size() / 3); ++i)
		{
			Vec3f n0 = {};
			n0[0] = attrib.normals[3 * i + 0];
			n0[1] = attrib.normals[3 * i + 1];
			n0[2] = attrib.normals[3 * i + 2];
			tmpVertexNormals.push_back(n0);
			//std::cout << i << " Tmp Normales: " << "(" << tmpVertexNormals[i][0] << "," << tmpVertexNormals[i][1] << "," << tmpVertexNormals[i][2] << ")" << "\n";
		}

		//Carga de coordenadas de textura, UVs
		for (uint32_t i = 0; i < (attrib.texcoords.size() / 2); ++i)
		{
			Vec2f uv0 = {};
			uv0[0] = attrib.texcoords[2 * i + 0];
			uv0[1] = attrib.texcoords[2 * i + 1];			
			tmpuv.push_back(uv0);
			//std::cout << i << " Tmp Normales: " << "(" << tmpVertexNormals[i][0] << "," << tmpVertexNormals[i][1] << "," << tmpVertexNormals[i][2] << ")" << "\n";
		}

		printf("# of vertices tmpVertices = %d\n", (int)(tmpVertices.size()));
		printf("# of vertex normals tmpVertexNormals = %d\n", (int)(tmpVertexNormals.size()));
		printf("# of uv coords tmpUVs = %d\n", (int)(tmpuv.size()));
		printf("# of face indices = %d\n", (int)(shapes[0].mesh.indices.size() / 3));
#ifdef MRT_DEBUG
		printf("\nIndices:\n");
#endif //MRT_DEBUG

		//exit(0);
		for (uint32_t i = 0; i < shapes.size(); ++i)
		{
			for (uint32_t j = 0; j < (shapes[i].mesh.indices.size() / 3); ++j)
				//shapes[0].mesh.indices.size();				
			{
				tinyobj::index_t idx0 = shapes[i].mesh.indices[3 * j + 0];
				tinyobj::index_t idx1 = shapes[i].mesh.indices[3 * j + 1];
				tinyobj::index_t idx2 = shapes[i].mesh.indices[3 * j + 2];
				tmpVertexIndices.push_back(shapes[i].mesh.indices[3 * j + 0].vertex_index);
				tmpVertexIndices.push_back(shapes[i].mesh.indices[3 * j + 1].vertex_index);
				tmpVertexIndices.push_back(shapes[i].mesh.indices[3 * j + 2].vertex_index);
				tmpNomalIndices.push_back(shapes[i].mesh.indices[3 * j + 0].normal_index);
				tmpNomalIndices.push_back(shapes[i].mesh.indices[3 * j + 1].normal_index);
				tmpNomalIndices.push_back(shapes[i].mesh.indices[3 * j + 2].normal_index);
				tmpUVIndices.push_back(shapes[i].mesh.indices[3 * j + 0].texcoord_index);
				tmpUVIndices.push_back(shapes[i].mesh.indices[3 * j + 1].texcoord_index);
				tmpUVIndices.push_back(shapes[i].mesh.indices[3 * j + 2].texcoord_index);

				int vi[3];      // indexes
				float v[3][3];  // coordinates

				for (int k = 0; k < 3; k++) 
				{
					vi[0] = idx0.vertex_index;
					vi[1] = idx1.vertex_index;
					vi[2] = idx2.vertex_index;
					assert(vi[0] >= 0);
					assert(vi[1] >= 0);
					assert(vi[2] >= 0);

					v[0][k] = attrib.vertices[3 * vi[0] + k];
					v[1][k] = attrib.vertices[3 * vi[1] + k];
					v[2][k] = attrib.vertices[3 * vi[2] + k];					

					//printf("\t(%d,%d,%d)\n", (int)(v[0][k]), (int)(v[1][k]), (int)(v[2][k]));					
				}
#ifdef MRT_DEBUG
				printf("\t%d Triangle (%d,%d,%d)\n", j, (int)(tmpVertexIndices[3 * j + 0]), (int)(tmpVertexIndices[3 * j + 1]), (int)(tmpVertexIndices[3 * j + 2]));
				printf("\t%d Normal (%d,%d,%d)\n", j, (int)(tmpNomalIndices[3 * j + 0]), (int)(tmpNomalIndices[3 * j + 1]), (int)(tmpNomalIndices[3 * j + 2]));
				printf("\t%d UV (%d,%d,%d)\n", j, (int)(tmpUVIndices[3 * j + 0]), (int)(tmpUVIndices[3 * j + 1]), (int)(tmpUVIndices[3 * j + 2]));
#endif // MRT_DEBUG

				
			}
		}
		std::vector<Vec3f> tmpFaceNormals = calcFaceNormals(tmpVertices, tmpVertexIndices);

#ifdef MRT_DEBUG
		printf("\t # Face Indices %d)\n", tmpVertexIndices.size() / 3);
		printf("\t # Vertex Normal Indices %d)\n", tmpNomalIndices.size() / 3);
		printf("\t # UV Indices %d)\n", tmpUVIndices.size() / 3);
		printf("\t # Face Normals %d)\n", tmpFaceNormals.size());
#endif // MRT_DEBUG

		return new TriangleMesh(tmpVertexIndices,
								tmpVertexIndices,
								tmpVertices,
								tmpFaceNormals,
								tmpVertexNormals,
								tmpuv);
		
			
		/*
		for (uint32_t i = 0; i < (attrib.texcoords.size()/2); ++i)
		{
			tmpuv[i][0] = attrib.normals[2 * i + 0];
			tmpuv[i][1] = attrib.normals[2 * i + 1];
			std::cout << " Tmp Coordenadas UV: " << "(" << tmpuv[i][0] << "," << tmpuv[i][1] << ")" << "\n";
		}

		for (uint32_t i = 0; i < shapes.size(); ++i)
		{
			for (uint32_t j = 0; j < (shapes[i].mesh.indices.size() / 3); ++j)
				//shapes[0].mesh.indices.size();				
			{

				//tinyobj::index_t idx0 = shapes[i].mesh.indices[3 * j + 0];
				//tinyobj::index_t idx1 = shapes[i].mesh.indices[3 * j + 1];
				//tinyobj::index_t idx2 = shapes[i].mesh.indices[3 * j + 2];

				//tmpVertices = 0;

					//_vertices[3 * j + 0] = shapes[i].mesh.indices[3 * j + 0];
			}
		}
		
		_faceIndex = ;
		_triangleIndex = ;
		_vertices = ;
		_faceNormals = ;
		_uv = ;
		*/
	}
	catch (...)
	{
		ifs.close();
	}
	

	return nullptr;
}//TriangleMesh::loadPolyMeshOBJ()



