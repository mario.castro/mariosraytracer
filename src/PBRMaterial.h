#pragma once
#include "../include/Eigen/Geometry"
#include "MRTMath.h"




class PBRMaterial
{
	PBRMaterial();
	~PBRMaterial();
	float diffuseBRDF();
	float specularBRDF();
	float cookTorranceBRDF();
	float ggx();
	float schlick();
	float ndf(const Vec3 &n, const Vec3 &h);

	Vec3 base;
	Vec3 albedo;	
	Vec3 normal;
	Vec3 metallic;	
	float F0;
	float roughness;
};

//
PBRMaterial::PBRMaterial()
{

}


//
PBRMaterial::~PBRMaterial()
{

}

//
float PBRMaterial::diffuseBRDF()
{
	return 0.0f;
}

//
float PBRMaterial::specularBRDF()
{
	return 0.0f;
}

//
float PBRMaterial::cookTorranceBRDF()
{
	return 0.0f;
}

//
float PBRMaterial::ggx()
{
	
	return 0.0f;
}

//
float PBRMaterial::ndf(const Vec3 &n, const Vec3 &h)
{
	float alpha2 = roughness * roughness;
	//Vec3 alpha2 = roughness * roughness;
	
	float dotNormHalf2 = n.dot(h);	

	float denom = (dotNormHalf2*dotNormHalf2*(alpha2 - 1) + 1);

	return (alpha2 / M_PI * denom);
	
}

//
float PBRMaterial::schlick()
{
	return 0.0f;
}

//
float PBRMaterial::schlick()
{
	return 0.0f;
}