#pragma once
#include "MRTMath.h"
//Sphere
const struct Sphere
{
	double rad; Vec3f p;
	Sphere(double r_, Vec3f p_) : rad(r_), p(p_) {}
	inline double intersect(const Ray& r) const
	{
		// ray-sphere intersection returns the distance
		const Vec3f op = p - r.o;
		double t, b = op.dotProduct(r.d), det = b * b - op.dotProduct(op) + rad * rad;
		if (det < 0)
		{
			return 1e20;
		}
		else
		{
			det = sqrt(det);
		}
		return (t = b - det) > 1e-4 ? t : ((t = b + det) > 1e-4 ? t : 1e20);
	}
};