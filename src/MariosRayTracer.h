#pragma once
#include <math.h>   // dirpole, directional dipole by T. Hachisuka and J. R. Frisvad
#include <stdlib.h> // originally smallpt, a path tracer by Kevin Beason, 2008
#include <stdio.h>  // Usage: ./dirpole 100000 && xv image.ppm
#include <iostream>
#include <chrono>
#include <ctime>
#include <algorithm>
#define STB_IMAGE_IMPLEMENTATION
#include "../include/stb/stb_image.h"
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "../include/stb/stb_image_write.h"
#include "../include/tiny_obj_loader.h"
#include "MRTMath.h"
#include "Object.h"
#include "Sphere.h"
#include "PolygonMesh.h"
#include "TriangleMesh.h"


static float kInfinity = std::numeric_limits<float>::max();
static float kEpsilon = 1e-8f;
static Vec3f kDefaultBackgroundColor(0.113f, 0.121f, 0.125f);


struct Options
{
	uint32_t width = 640;
	uint32_t height = 480;
	uint32_t samples = 10000;
	float fov = 90;
	Vec3f backgroundColor = kDefaultBackgroundColor;
	Matrix4f cameraToWorld;
	std::string imageFileName = "image";	
};




