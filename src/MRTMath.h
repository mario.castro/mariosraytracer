#pragma once
//#define MRT_EIGEN 

#ifdef MRT_EIGEN
#include "../include/Eigen/Dense"
using namespace Eigen;

typedef  Vector3f Vec3f;
typedef  Vector2f Vec2f;
typedef  Eigen::Matrix4f Matrix4f;

#else

#include "MRTGeometry.h"
//typedef Vec3<float> Vec3f;
//typedef Vec2<float> Vec2f;
//typedef Vec3<int> Vec3;
//typedef Vec2<int> Vec2;
typedef Matrix44<float> Matrix4f ;
#endif

#define M_PI       3.14159265358979323846  /* pi */
#define M_PI_2     1.57079632679489661923  /* pi/2 */
#define deg2rad(angleDegrees) (angleDegrees * M_PI / 180.0)


struct Ray
{
	Vec3f o, d;
	Ray() {};
	Ray(Vec3f o_, Vec3f d_) : o(o_), d(d_) {}
};