
#include "MRTMath.h"
__declspec(align(16)) class Object
{
public:
	Object() {}
	virtual ~Object() {}
	virtual bool intersect(const Vec3f &o, const Vec3f &d, float &tNear, const uint32_t &vidx, const uint32_t &fidx, Vec2f &uv) const = 0;
	virtual bool intersectBarycentric(const Vec3f &o, const Vec3f &d, float &tNear, const uint32_t &vidx, const uint32_t &fidx, Vec2f &uv) const;
	virtual Vec3f* traceRayToObject(const Ray &ray, float &tNear) const = 0;
	virtual void getSurfaceProperties(const Vec3f &, const Vec3f &, const uint32_t &, const Vec2f &, Vec3f &, Vec2f &) const = 0;
	Matrix4f objectToWorld, worldToObject;
};

