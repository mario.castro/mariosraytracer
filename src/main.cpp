//#define _CRT_SECURE_NO_WARNINGS
//#define MRT_DEBUG
#include "MariosRayTracer.h"



Vec3f* trace_rays(const Ray &ray, 
				const std::vector<Object*> &objects,
				uint32_t i, 
				Options &options)
{
	float tNear = 150.0f;
	
	for (size_t h = 0; h < objects.size(); h++)
	{
		if (objects[h]->traceRayToObject(ray, tNear))
			return new Vec3(1.0f,1.0f,1.0f);
		else
			return new Vec3(0.0f, 0.0f, 0.0f);
	}
				
	
	return nullptr;
}

int main(int argc, char* argv[])
{
		
	Options options;
	options.width = (argc >= 4) ? atoi(argv[2]) : 256;
	options.height = (argc >= 4) ? atoi(argv[3]) : 256;
	options.imageFileName = (argc >= 4) ? argv[4] : "image";
	options.samples  = (argc == 2) ? atoi(argv[1]) : 1 << 13;
	Matrix44f tmp(0.707107f, -0.331295f, 0.624695f, 0.0,
			0.0f, 0.883452f, 0.468521f, 0.0f,
			-0.707107f,-0.331295f, 0.624695f, 0.0,
			-1.63871f, -5.747777f, -40.400412f, 1.0f);
	options.cameraToWorld = tmp.inverse();
	options.fov = 50.0393f;

	std::vector<Object*> objects;
	std::string meshPath = (argc == 6) ? argv[5] : "F:\\Projects\\MariosRayTracer\\bin\\Triangle.obj";
	TriangleMesh *mesh = TriangleMesh::loadPolyMeshOBJ(meshPath.c_str());
	if(mesh != nullptr) objects.push_back(mesh);
	printf("Muestras: %d", options.samples);
	//exit(0);
	//Rayo desde la camara
	//const Ray cam(Vec3(50.0f, 45.0f, 290.0f), Vec3(0.0f, -0.042612f, -1.0f).normalized());	
	const Ray cam(Vec3f(0.0f, 0.0f, 0.0f), Vec3(0.0f, 0.0f, -1.0f).normalize());
	const  Vec3f cx = Vec3f(options.width * 0.25 / options.width), cy = (cx.crossProduct(cam.d)).normalize() * 0.25;
	std::vector<Vec3f*> c(options.width * options.height);
	
	//Inicia medicion de tiempo de ejecucion
	std::chrono::time_point<std::chrono::system_clock> start, end;
	start = std::chrono::system_clock::now();
	
	Vec3f d;
	float scale = (float)tan(deg2rad(options.fov * 0.5));
	float imageAspectRatio = options.width / (float)options.height;

	Vec3f o;

	for (uint32_t y = 0; y < options.height; y++)
	{
		fprintf(stderr, "\rRenderizando %5.2f%%", 100.0 * y / (options.height - 1));
		for (uint32_t x = 0; x < options.width; x++)
		{			
			const int i = x + y * options.width;
			
			float pixelX = (float)(2 *((x + 0.5) / options.width - 1)) * imageAspectRatio * scale;
			float pixelY = (float)(1 - 2 * ((y + 0.5) / options.height)) * scale;
						
			//const Vec3f d = cx * ((x + 0.5) / options.width - 0.5) + cy * (-(y + 0.5) / options.width + 0.5) + cam.d;
			
			c[i] = trace_rays(Ray(cam.o + d * 140, d.normalize()),objects, i, options);
			c[i] = trace_rays(cam, objects, i, options);
			//objects[h]->traceRayToObject(ray.o, ray.d.normalized(), 150.0);
		}
	}
	//Termina ejecucionalgoritmo
	end = std::chrono::system_clock::now();

	//Tiempo transcurrido
	long elapsed_seconds = std::chrono::duration_cast<std::chrono::seconds>
		(end - start).count();


	// Imagen en formato PFM (en HDR)
	float *cf = new float[options.width * options.height * 3];
	int *crgb = new int[options.width * options.height * 3];
	for (uint32_t i = 0; i < options.width * options.height; i++)
	{
		
		cf[i * 3 + 0] = c[i]->x;
		cf[i * 3 + 1] = c[i]->y;
		cf[i * 3 + 2] = c[i]->z;
		//crgb[i * 3 + j] = int(255.99*c[i][j]);
		//std::cout << c[i][j] <<" ";
		
		std::cout << "\n";
	}

	std::string fileFormat = ".pfm";
	//std::string options.imageFileName; 
	options.imageFileName += fileFormat;
	//options.imageFileName += "x" + h;
	//options.imageFileName += "S" + options.samples;
	//options.imageFileName += ".pfm";

	//Archivo PFM
	FILE *f = fopen(options.imageFileName.c_str(), "wb");
	fprintf(f, "PF\n%d %d\n%6.6f\n", options.width, options.height, -1.0);
	fwrite(cf, options.width * options.height * 3, sizeof(float), f);
	//std::cout <<"Cantidad de muestras: "<<options.samples<< ", Tiempo transcurrido: " << elapsed_seconds;
	printf("Tiempo transcurrido:%d", elapsed_seconds);
	stbi_write_bmp((options.imageFileName + ".bmp").c_str(), options.width, options.height, 3, crgb);

	// save framebuffer to file
	char buff[256];
	short frame = 0;
	sprintf(buff, "out.%04d.ppm", frame);
	std::ofstream ofs;
	ofs.open(buff);
	ofs << "P6\n" << options.width << " " << options.height << "\n255\n";
	for (uint32_t i = 0; i < options.height * options.width; ++i) {
		char r = (int)(255.99 *  c[i]->x);
		char g = (int)(255.99 *  c[i]->y);
		char b = (int)(255.99 *  c[i]->z);
		ofs << r << g << b;
	}
	ofs.close();

}